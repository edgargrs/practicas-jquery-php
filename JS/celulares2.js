$(document).ready(inicio);

function inicio(){
	var buscar = $("#txtbuscar").val();

	$("#boton").click(cargaCelular);
	$("#motorola").click(motorola);
	$("#samsung").click(samsung);
	$("#sony").click(sony);
	$("#mac").click(mac);
	$("#btnbuscar").click(cargaBusqueda2);
}

function cargaBusqueda(){
	var url="celularBusqueda.php";
	$.ajax({
		type: "POST",
		url: url,
		data: $("#frmbuscar").serialize(),
		// Adjuntar los campos del
		// formulario enviado
		success:  function(data){
			$("#contenedor").html(data);

		}
	});
	return false;
}


function cargaBusqueda2(){
	var buscar = $("#txtbuscar").val();
	$("#contenedor").load("celularBusqueda.php?buscar="+buscar);
}


function cargaCelular(){
	$("#contenedor").load("celular.php?marca=");
}

function motorola(){
	$("#contenedor").load("celular.php?marca=Motorola");
}

function samsung(){
	$("#contenedor").load("celular.php?marca=Samsung");
}

function sony(){
	$("#contenedor").load("celular.php?marca=Sony");
}

function mac(){
	$("#contenedor").load("celular.php?marca=Mac");
}