$(document).ready(inicio);

function inicio(){

	$("#uno").click(uno);
	$("#dos").click(dos);
	$("#tres").click(tres);
	$("#cuatro").click(cuatro);
	$("#cinco").click(cinco);
	$("#seis").click(seis);
	$("#siete").click(siete);
	$("#ocho").click(ocho);
	$("#nueve").click(nueve);
	$("#diez").click(diez);	
}

function uno(){
	alert($("#uno").attr("id")); /*Sirve para mostrar el contenido*/
	/*$("#uno").attr("id","once"); /*cambia el valor por once*/
}

function dos(){
	alert($("#dos").attr("id")); /*Sirve para mostrar el contenido*/
	/*$("#dos").attr("id","once"); /*cambia el valor por once*/
}

function tres(){
	alert($("#tres").attr("id")); /*Sirve para mostrar el contenido*/
	/*$("#tres").attr("id","once"); /*cambia el valor por once*/
}

function cuatro(){
	alert($("#cuatro").attr("id")); /*Sirve para mostrar el contenido*/
	/*$("#cuatro").attr("id","once"); /*cambia el valor por once*/
}

function cinco(){
	alert($("#cinco").attr("id")); /*Sirve para mostrar el contenido*/
	/*$("#cinco").attr("id","once"); /*cambia el valor por once*/
}

function seis(){
	alert($("#seis").attr("id")); /*Sirve para mostrar el contenido*/
	/*$("#seis").attr("id","once"); /*cambia el valor por once*/
}

function siete(){
	alert($("#siete").attr("id")); /*Sirve para mostrar el contenido*/
	/*$("#siete").attr("id","once"); /*cambia el valor por once*/
}

function ocho(){
	alert($("#ocho").attr("id")); /*Sirve para mostrar el contenido*/
	/*$("#ocho").attr("id","once"); /*cambia el valor por once*/
}

function nueve(){
	alert($("#nueve").attr("id")); /*Sirve para mostrar el contenido*/
	/*$("#nueve").attr("id","once"); /*cambia el valor por once*/
}

function diez(){
	alert($("#diez").attr("id")); /*Sirve para mostrar el contenido*/
	/*$("#diez").attr("id","once"); /*cambia el valor por once*/
}
