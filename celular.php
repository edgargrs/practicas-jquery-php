<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Celular</title>
	<link rel="stylesheet" href="CSS/celulares1.css">
</head>
<body>

<?php 
	include "script/php/ConexionPDO.php";

	$conn = new ConexionPDO("127.0.0.1","root","123","Celular"); //Objeto de conexción

	$sql="select * from Celular where marca like '%".$buscar."%'";
	if($conn->realizarConexion()){

		$resultado=$conn->hacerConsulta($sql);
		foreach ($resultado as $valores) {
			?>

				<div class="celular">
					<div class="titulo">
						<h1> <?php echo $valores['Marca']; ?> </h1>
					</div>
					<div class="contenido">
						<div class="imagen">
							<img src="<?php echo $valores['Imagen']; ?>" alt="">
						</div>
						<div class="descripcion">
							<ul>
								<li>Marca: <?php echo $valores['Marca']; ?> </li>
								<li>Modelo: <?php echo $valores['Modelo']; ?></li>
								<li>Sistema Operativo: <?php echo $valores['Sistema_Operativo']; ?> </li>
								<li>Color: <?php echo $valores['Color']; ?> </li>
								<li>Pantalla: <?php echo $valores['Pantalla']; ?></li>
								<li>Camara: <?php echo $valores['Camara']; ?></li>
							</ul>
						</div>
						<div class="precio">
							<h1>Precio: $<?php echo $valores['Precio']; ?></h1>
						</div>
					</div>

					
				</div>

			<?php
		}
	}	
?>
</body>
</html>