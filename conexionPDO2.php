<?php
	class ConexionPDO2{
		private $server;
		private $user;
		private $password;
		private $baseDatos;
		private $conexion;

		function ConexionPDO2(){
			$argumentos=func_get_args();
			if(count($argumentos)!=0){
				$this->server=$argumentos[0];
				$this->user=$argumentos[1];
				$this->password=$argumentos[2];
				$this->baseDatos=$argumentos[3];
			}else{
				include "incluir/config.php";
				$this->server=${$var1};
				$this->user=${$var2};
				$this->password=${$var3};
				$this->baseDatos=${$var4};
			}
		}

		public function realizarConexion(){
			try {
    				$this->conexion =
    				new PDO("mysql:host=$this->server;dbname=$this->baseDatos",
    				$this->user, $this->password);
    				$this->conexion->setAttribute
                    (PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    				//echo "Conexion OK";
					return  true;
    		}
			catch(PDOException $e){
    			echo "La conexión no se pudo realizar: " . $e->getMessage();
    		}
		}
		public function crearEncabezadosTabla(){
			$argumentos=func_get_args();
			echo "<tr>";
			for ($i=0; $i<count($argumentos) ; $i++) {
				?>
				<th><?php echo $argumentos[$i]; ?></th>
				<?php
			}
			?>
			<th></th><th></th>
			<?php
			echo "</tr>";
		}


		public function crearCheckbox($tabla,$nombre){
			$sql="select * from ".$tabla;
			try{
				$rows=$this->hacerConsulta($sql);
				foreach ($rows as $value) {
					foreach ($value as $key=>$value2) {
						$valor[]=$value2;
					}
				}
				for($i=0;$i<count($valor);$i++){
					if($i%2==0){
						$indices[]=$valor[$i];
					}else if($i%2==1){
						$valOp[]=$valor[$i];
					}
				}
				for($i=0;$i<count($indices);$i++){
					?>
					<input type="checkbox" name="<?php echo $nombre."[]"; ?>" value="<?php echo $indices[$i]; ?>" /><?php echo $valOp[$i]; ?><br />
					<?php
				}
			}//fin de try

			catch(PDOException $e){
    			echo "La consulta no se pudo realizar: " . $e->getMessage();
    		}
		}

		public function hacerConsulta($sql){
			try{
				$resultado=$this->conexion->query("SET NAMES 'utf8'");
				$resultado=$this->conexion->query($sql);
				$rows=$resultado->fetchAll(PDO::FETCH_ASSOC);
				return $rows;
			}catch(PDOException $e){
    			echo "La consulta no se pudo realizar: " . $e->getMessage();
    		}
		}

		public function probarLogin($sql){
			try{
				$resultado=$this->conexion->query("SET NAMES 'utf8'");
				$resultado=$this->conexion->query($sql);
				return $resultado->rowCount();
			}catch(PDOException $e){
    			echo "La consulta no se pudo realizar: " . $e->getMessage();
    		}
		}

		public function probarLoginEnc($sql){
			try{
				$resultado=$this->conexion->query("SET NAMES 'utf8'");
				$resultado=$this->conexion->query($sql);
				return $resultado->rowCount();
			}catch(PDOException $e){
    			echo "La consulta no se pudo realizar: " . $e->getMessage();
    		}
		}


		public function tipoUsuario($usuario, $pass){
			try{//inyeccion sql
				$sql="select tipo from log where user='".$usuario."' and password='".$pass."'";

				$resultado=$this->conexion->query("SET NAMES 'utf8'");
				$resultado=$this->conexion->query($sql);
				foreach($resultado as $valor){
	                $resultado=$valor['tipo'];
	            }
				return $resultado;
			}catch(PDOException $e){
				echo "La consulta no se pudo realizar: " . $e->getMessage();
			}
		}

		public function crearLista($tabla,$nombreLista,$idLista=""){
			$sql="select * from ".$tabla;
			try{
				$resultado=$this->conexion->query("SET NAMES 'utf8'");
				$resultado=$this->conexion->query($sql);
				$rows=$resultado->fetchAll(PDO::FETCH_ASSOC);
				?>
				<select id="<?php echo $idLista; ?>" name="<?php echo $nombreLista; ?>">
				<?php
				foreach ($rows as $value) {
					foreach ($value as $key=>$value2) {
							$valor[]=$value2;
					}
				}

				for($i=0;$i<count($valor);$i++){
					if($i%2==0){
						$indices[]=$valor[$i];
					}else if($i%2==1){
						$valOp[]=$valor[$i];
					}
				}
				for($i=0;$i<count($indices);$i++){
				?>
					<option value="<?php echo $indices[$i];?>"><?php echo $valOp[$i];?></option>
				<?php
				}
				?>
				</select>
				<?php
			}catch(PDOException $e){
    			echo "La consulta no se pudo realizar: " . $e->getMessage();
    		}
		}

		public function crearReporteEncabezadosTabla(){
			$argumentos=func_get_args();
			echo "<table><tr>";
			for ($i=0; $i<count($argumentos) ; $i++) {
				?>
				<th><?php echo $argumentos[$i]; ?></th>
				<?php
			}
			echo "</tr>";
		}

		public function crearTabla($tabla){
			$sql="select * from ".$tabla;
			$resultado=$this->conexion->query("SET NAMES 'utf8'");
			$resultado=$this->conexion->query($sql);
			$rows=$resultado->fetchAll(PDO::FETCH_ASSOC);
			$contador=0;
			$id="";
			foreach ($rows as $key => $value) {
				?>
				<tr>
				<?php
				foreach ($value as $value2) {
					if($contador==0){
						$id=$value2;
						$contador++;
					}
					?>
					<td><?php echo $value2; ?></td>
					<?php
				}
				$contador=0;
				?>
				<td><img id="<?php echo $id; ?>" src="imagenes/borrar.png" /></td>
				<td><img id="<?php echo $id; ?>" src="imagenes/modificar.png" /></td>
				</tr>
				<?php
			}
			echo "</table>";
		}

		public function reporteTabla($sql){
			try{
				$resultado=$this->conexion->query("SET NAMES 'utf8'");
				$resultado=$this->conexion->query($sql);
				$rows=$resultado->fetchAll(PDO::FETCH_ASSOC);
				foreach ($rows as $key => $value) {
					echo "<tr>";
					foreach ($value as $key2 => $value2) {
						?>
						<td><?php echo $value2; ?></td>
						<?php
					}
					echo "</tr>";
				}
				echo "</table>";
			}catch(PDOException $e){
				echo "Error en la consulta...<br />".$e->getMessage();
			}

		}

		public function crearTablaMETA($tabla){
			$sql="select * from ".$tabla;
			try{
				$resultado=$this->conexion->query("SET NAMES 'utf8'");
			$resultado=$this->conexion->query($sql);
			$rows=$resultado->fetchAll(PDO::FETCH_ASSOC);
			$contador=0;
			$contador2=0;
			$id="";

			foreach ($rows as $value) {
				if($contador2==0){
					echo "<table><tr>";
					foreach ($value as $indice=>$value2) {
						?>
						<th><?php echo $indice;?></th>
						<?php
						$contador2++;
					}
					echo "<th></th><th></th></tr>";
				}
				echo "<tr>";
				foreach ($value as $indice=>$value2) {
					if($contador==0){
						$id=$value2;
						$contador++;
					}
					?>
					<td><?php echo $value2; ?></td>
					<?php
				}
				$contador=0;
				?>
				<td class="tdFinal"><img class="iciono" id="<?php echo $id; ?>" src="imagenes/borrar.png" /></td>
				<td class="tdFinal"><img class="iciono" id="<?php echo $id; ?>" src="imagenes/modificar.png" /></td>
				</tr>
				<?php
			}
			echo "</table>";
			}//fin de try
			catch(PDOException $e){
				echo "Error en la consulta...<br />".$e->getMessage();
			}

		}
		

		public function insertarUsuario($usuario,$password,$tipo){
			try{
				$statement =$this->conexion->prepare("
				insert into log(user,password,tipo)
				values(:us,:pass,:ti)
				");
				$statement->bindParam(':us',$us);
				$statement->bindParam(':pass',$pass);
				$statement->bindParam(':ti',$ti);
				$us=$usuario;
				$pass=$password;
				$ti=$tipo;
				$statement->execute();
				echo "<br />Datos agregados correctamente... ";
			}catch(PDOException $e){
				echo "Error en la consulta...<br />".$e->getMessage();
			}
		}

		public function insertarAlumnoBind($pMatricula,$pNombre,$pApp,$pEdad,$pGenero,$pInstitucion){
			try{
				$statement =$this->conexion->prepare(
				"insert into alumno(matricula,nombre,apps,edad,genero,institucion)
				values(:mat, :nom, :app, :ed, :gen, :institucion)");
				$statement->bindParam(':mat',$matricula);
				$statement->bindParam(':nom',$nombre);
				$statement->bindParam(':app',$apellidos);
				$statement->bindParam(':ed',$edad);
				$statement->bindParam(':gen',$genero);
				$statement->bindParam(':institucion',$institucion);

				$matricula=$pMatricula;
				$nombre=$pNombre;
				$apellidos=$pApp;
				$edad=$pEdad;
				$genero=$pGenero;
				$institucion=$pInstitucion;

				$statement->execute();
				echo "<br />Datos agregados correctamente... ";
			}catch(PDOException $e){
				echo "Error en la consulta...<br />".$e->getMessage();
			}
		}
	}
?>
