<!DOCTYPE html>
<html lang="en">
<head>	
	<meta charset="UTF-8">
	<title>Celular con barra</title>
	<link rel="stylesheet" href="CSS/celulares2.css">	
	<script src="JS/jquery.js"></script>	
</head>
<body>
	<div class="centrar">
		<div class="barra"> 
			<div class="boton" id="boton">Celulares</div>
			<div class="boton" id="motorola">Motorola</div>
			<div class="boton" id="samsung">Samsung</div>
			<div class="boton" id="sony">Sony</div>
			<div class="boton" id="mac">Mac</div>
			<div id="divBuscar">
				<form action="" id="frmbuscar">
					<input name="btnbuscar" type="button" value="Buscar" id="btnbuscar">
					<input name="txtbuscar" type="search" placeholder="Buscar" id="txtbuscar">		
				</form>				
			</div>
			<div id="contenedor">

			</div>
		</div>	
	<script src="JS/celulares2.js"></script>	
</body>
</html>